const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

/* 
Tasks:

1.Returns a copy of the object where the keys have become
the values and the values the keys.
2.Assume that all of the object's values will be unique and string serializable.
3. http://underscorejs.org/#invert

*/

function invert(obj) {
  const new_obj = {};
  for (key in obj) {
    new_obj[obj[key]] = key;
  }
  console.log(new_obj);
}

module.exports = { invert };

// invert(testObject);
