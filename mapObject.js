const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

/* 
Tasks:

1.Like map for arrays, but for objects. Transform the value of each property
in turn by passing it to the callback function.
2.http://underscorejs.org/#mapObject

*/

function helper(value) {
  return value + 5;
}

function mapObject(obj, cb) {
  for (key in obj) {
    if (key == "age") {
      obj[key] = cb(obj[key]);
    }
  }
  return obj;
}

module.exports = { mapObject, helper };

// console.log(mapObject(testObject, helper));
