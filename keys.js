const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

/* 
Tasks:

1.Retrieve all the names of the object's properties.
2.Return the keys as strings in an array.
3.Based on http://underscorejs.org/#keys
*/

function keys(obj) {
  const key_arr = [];

  for (key in obj) {
    key_arr.push(key);
  }

  return key_arr;
}

module.exports = { keys };

// console.log(keys(testObject));
