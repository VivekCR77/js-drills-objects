const { defaults } = require("../defaults.js");

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

defaults(testObject, {
  name: "Vivek Dubey",
  age: 20,
  location: "Mumbai",
  Company: "Mount Blue",
  Designation: "Software Engineer",
});
