const { invert } = require("../invert.js");

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

invert(testObject);
