const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

/* 
Tasks:

1.Fill in undefined properties that match properties on the
`defaultProps` parameter object.
2.Return `obj`.
3. http://underscorejs.org/#defaults

*/

function defaults(obj, defaultProps) {
  for (key in defaultProps) {
    if (!(key in obj)) {
      obj[key] = defaultProps[key];
    } else {
      obj[key] = defaultProps[key];
    }
  }
  console.log(obj);
}

module.exports = { defaults };

// defaults(testObject, {
//   name: "Vivek Dubey",
//   age: 20,
//   location: "Mumbai",
//   Company: "Mount Blue",
//   Designation: "Software Engineer",
// });
