const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

/* 
Tasks:

1.Return all of the values of the object's own properties.
2.Ignore functions
3.http://underscorejs.org/#values
*/

function values(obj) {
  const value_arr = [];

  for (key in obj) {
    value_arr.push(obj[key]);
  }
  return value_arr;
}

module.exports = { values };

// console.log(values(testObject));
