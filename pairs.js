const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

/* 
Tasks:

1.Convert an object into a list of [key, value] pairs.
2.http://underscorejs.org/#pairs

*/

function pairs(obj) {
  const output_arr = [];

  for (key in obj) {
    output_arr.push([key, obj[key]]);
  }

  return output_arr;
}

module.exports = { pairs };

// console.log(pairs(testObject));
